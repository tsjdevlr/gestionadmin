<?php

namespace App\Controller;

use App\Entity\Movimiento;
use App\Entity\Expediente;
use App\Entity\Firma;
use App\Form\MovimientoType;
use App\Form\MovimientoEditType;
use App\Form\PaseFirmadoType;
use App\Form\Movimiento\FiltroType;
use App\Repository\MovimientoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/admin/movimiento")
 */
class MovimientoController extends AbstractController
{
    /**
     * @Route("/", name="movimiento_index", methods={"GET"})
     */
    public function index(Request $request, MovimientoRepository $movimientoRepository, PaginatorInterface $paginator): Response
    {
        // Creo el formulario del filtro.
        // Luego consulto si viene el formulario en el $request para
        // asociar esos valores en el formulario
        $formFiltro = $this->createForm(FiltroType::class);
        if ($request->query->get($formFiltro->getName())) {
            $formFiltro->handleRequest($request);
        }

        // Creo el $queryBuilder a trevés del cual se proporcionaran las entidades para
        // ser listadas. Como parametro paso el formulario con los filtros seleccionados
        $queryBuilder = $movimientoRepository->findForActionIndex($formFiltro->getData());
        $pagination = $paginator->paginate(
          $queryBuilder,
          $request->query->get('page', 1),
          12
        );

        return $this->render('movimiento/index.html.twig', [
          'pagination' => $pagination,
          'formFiltro' => $formFiltro->createView(),
        ]);
    }

    /**
     * @Route("/new", name="movimiento_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        // Capturamos el id del movimiento anterior
        $idSeg = $request->query->get('idSeg');
        // Obtener el id del expediente
        $id_exp = $request->query->get('exp');
        // Obtenemos el expediente
        $em = $this->getDoctrine()->getManager();   
        $exp = $em->getRepository(Expediente::class)->find($id_exp);

        $movimiento = new Movimiento();
        $form = $this->createForm(MovimientoType::class, $movimiento);
        $form->handleRequest($request);          
        
        // Si el usuario tiene "Area", traemos las firmas de las misma
        if($this->getUser()->getArea()){
            $firmas = $em->getRepository(Firma::class)->findByArea($this->getUser()->getArea()->getId());
        }else{
            $firmas = null;
        }        

        if ($form->isSubmitted() && $form->isValid()) {
            
            // preguntar si el movimiento es enviado a la misma area   
            if($movimiento->getArea() != $this->getUser()->getArea()) {
                // Pregutamos si exite un movimiento del cual se realizo el pasa nuevo
                if($idSeg){                  
                    // Guardamos la fecha actual en el campo fechaSalida y el estado de salido del movimiento 
                    $seguimiento = $em->getRepository(Movimiento::class)->find($idSeg);
                    $seguimiento->setFechaEgreso(new \DateTime());
                    $seguimiento->setEstado($movimiento->getEstado());
                    $seguimiento->setUbicacion(0);
                    //$seguimiento->setObservacion($movimiento->getObservacion());
                    $em->persist($seguimiento);
                    $em->flush();
                }
                
                $movimiento->setExpediente($exp); 
                $movimiento->setFechaIngreso(new \DateTime());

                // Preguntamos si el pase viene de un movimiento y si es asi, si el estado del movimiento es Archivado o Finalizado
                if($idSeg and ($seguimiento->getEstado() == 'Archivado' or $seguimiento->getEstado() == 'Finalizado')){
                    $movimiento->setEstado($seguimiento->getEstado());
                }else{
                    $movimiento->setEstado('En Revisión');
                }
                $movimiento->setUbicacion(1);
                $movimiento->setUser($this->getUser());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($movimiento);
                $entityManager->flush();

                // guardamos el pase en pdf en el directorio pases 
                $dir = $this->getParameter('pases_directory');
                if(!is_dir($dir)){
                    mkdir($dir);
                }
                $routeFile = $dir."Pase_N_".$movimiento->getId();   
                         
                if(file_exists($routeFile)){
                    unlink($routeFile);
                }
                if($request->request->get('viene') == 'si'){
                    $firma = $em->getRepository(Firma::class)->find($request->request->get('firmado'));
                    $routeFile = '../../public/uploads/firmas/';
                    $firmaFile = $routeFile.$firma->getPersona()->getFirma();
                    // Retrieve the HTML generated in our twig file
                    $html = $this->renderView('expediente/print/expediente_print_pase.html.twig', [
                        'title' => "Caratula de Expediente",
                        'expediente' => $movimiento->getExpediente(),
                        'pase' => $movimiento,
                        'firma' => $firmaFile,
                    ]);
                }else{
                    // Retrieve the HTML generated in our twig file
                    $html = $this->renderView('expediente/print/expediente_print_pase.html.twig', [
                        'title' => "Caratula de Expediente",
                        'expediente' => $exp,
                        'pase' => $movimiento
                    ]);
                }
                
                $numero = $movimiento->getId();

                // Configure Dompdf according to your needs
                $pdfOptions = new Options();
                $pdfOptions->set('defaultFont', 'Arial');
                $pdfOptions->setIsRemoteEnabled(true);
                // Instantiate Dompdf with our options
                $dompdf = new Dompdf($pdfOptions);       

                
                $html .= '<link type="text/css" href="../assets/templates/templates.css" rel="stylesheet" />';
                $nombrepdf="Pase_N_".$numero;   

                // Load HTML to Dompdf
                $dompdf->loadHtml($html);
                
                // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
                $dompdf->setPaper('A4', 'portrait');

                // Render the HTML as PDF
                $dompdf->render();       
                // Almacenar datos binarios PDF
                $output = $dompdf->output();
                        
                // En este caso, queremos escribir el archivo en el directorio público.
                $pdfFilepath =  $dir . $nombrepdf;
                
                // Escriba el archivo en la ruta deseada
                file_put_contents($pdfFilepath, $output);
                /*
                $dompdf->stream($nombrepdf.".pdf", [
                    "Attachment" => false
                ]);
                */
                $this->addFlash('success', '¡El Expediente fue movido a "'.$movimiento->getArea()->getNombre().'" correctamente!');
                return $this->redirectToRoute('expediente_show', ['id'=>$id_exp]);
            }else{
                $this->addFlash('danger', '¡No puede enviar el Expediente la misma area!');
                return $this->redirectToRoute('movimiento_new', ['exp'=>$id_exp, 'idSeg'=>$idSeg]);
            }
            
            
        }

        return $this->render('movimiento/new.html.twig', [
            'movimiento' => $movimiento,
            'form' => $form->createView(),
            'firmas' => $firmas,
            'id' => $id_exp,
        ]);
    }

    /**
     * @Route("/{id}", name="movimiento_show", methods={"GET"})
     */
    public function show(Movimiento $movimiento, Request $request): Response
    {
        
        $id_exp = $request->query->get('exp');
        $area = $request->query->get('area');
        
        $em = $this->getDoctrine()->getManager();   
        $exp = $em->getRepository(Expediente::class)->find($id_exp);

        return $this->render('movimiento/show.html.twig', [
            'movimiento' => $movimiento,
            'exp' => $exp,
            'area' => $area,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="movimiento_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Movimiento $movimiento): Response
    {
        //$exp = $request->query->get('exp');
        $exp = $movimiento->getExpediente();
        $form = $this->createForm(MovimientoEditType::class, $movimiento);
        $form->handleRequest($request);
        // Si el usuario tiene "Area", traemos las firmas de las misma
        $em = $this->getDoctrine()->getManager();
        if($this->getUser()->getArea()){
            $firmas = $em->getRepository(Firma::class)->findByArea($this->getUser()->getArea()->getId());
        }else{
            $firmas = null;
        } 
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Se actualizo movimiento '.' correctamente!');

            // borramos pase viejo
            $routeFile = $this->getParameter('pases_directory')."Pase_N_".$movimiento->getId();               
            if(file_exists($routeFile)){
                unlink($routeFile);
            }

            // creamos nuevo pase en pdf                  
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');
            $pdfOptions->setIsRemoteEnabled(true);
            // Insataciamos Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);       

            // Retrieve the HTML generated in our twig file
            $html = $this->renderView('expediente/print/expediente_print_pase.html.twig', [
                'title' => "Caratula de Expediente",
                'expediente' => $exp,
                'pase' => $movimiento
            ]);
            $html .= '<link type="text/css" href="../assets/templates/templates.css" rel="stylesheet" />';
            $nombrepdf="Pase_N_".$movimiento->getId();   

            // Load HTML to Dompdf
            $dompdf->loadHtml($html);
            
            // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
            $dompdf->setPaper('A4', 'portrait');

            // Render the HTML as PDF
            $dompdf->render();
            
            // Almacenar datos binarios PDF
            $output = $dompdf->output();
            
            // En este caso, queremos escribir el archivo en el directorio público.
            $publicDirectory = $this->getParameter('pases_directory');
            $pdfFilepath =  $publicDirectory . $nombrepdf;
            
            // Escriba el archivo en la ruta deseada
            file_put_contents($pdfFilepath, $output);

            return $this->redirectToRoute('expediente_show', ['id'=>$exp->getId()]);
        }
        return $this->render('movimiento/edit.html.twig', [
            'movimiento' => $movimiento,
            'form' => $form->createView(),
            'exp' => $exp,
            'firmas' => $firmas,
        ]);
    }

    /**
     * @Route("/{id}/", name="movimiento_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Movimiento $movimiento): Response
    {
        //Traigo el id de la expediente
        $exp = $movimiento->getExpediente()->getId();
        if ($this->isCsrfTokenValid('delete'.$movimiento->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            
            // eliminamos el pase
            $routeFile = $this->getParameter('pases_directory')."Pase_N_".$movimiento->getId();               
            if(file_exists($routeFile)){
                unlink($routeFile);
            }
            // preguntamos si el movimiento tiene un archivo asociado
            if($movimiento->getArchivo()){
                // eliminamos el archivo del movimiento
                $routeFile = $this->getParameter('movimientos_directory').$movimiento->getArchivo();               
                unlink($routeFile);
            }                       

            $entityManager->remove($movimiento);
            $entityManager->flush();
            $this->addFlash('success', '¡Registro eliminado correctamente!');

            $expediente = $entityManager->getRepository(Expediente::class)->find($exp);

            $cant = count($expediente->getMovimientos());
            $i=1;
            foreach($expediente->getMovimientos() as $m){
                if($cant == $i){
                    $movimiento = $entityManager->getRepository(movimiento::class)->find($m->getId());
                    $movimiento->setEstado('En Revisión');
                    $movimiento->setFechaEgreso(null);
                    $entityManager->persist($movimiento);
                    $entityManager->flush();
                }
                $i++;
            }
        }

        return $this->redirectToRoute('expediente_show', ['id'=>$exp]);
    }
}
