<?php

namespace App\Controller;

use App\Entity\Expediente;
use App\Entity\Movimiento;
use App\Entity\Area;
use App\Entity\Persona;
use App\Form\ExpedienteType;
use App\Form\Expediente\FiltroType;
use App\Form\Expediente\FiltroSecAdminType;
use App\Form\Expediente\FiltroEstadisticasType;
use App\Repository\ExpedienteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
// Include Dompdf required namespaces
    use Dompdf\Dompdf;
    use Dompdf\Options;
use iio\libmergepdf\Merger;
use iio\libmergepdf\Pages;

/**
 * @Route("/admin/expediente")
 */
class ExpedienteController extends AbstractController
{
    /**
     * @Route("/", name="expediente_index", methods={"GET"})
     */
    public function index(Request $request, ExpedienteRepository $expedienteRepository, PaginatorInterface $paginator): Response
    {
        // Creo el formulario del filtro.
        // Luego consulto si viene el formulario en el $request para
        // asociar esos valores en el formulario
        
        if(in_array("ROLE_SUPER_ADMIN", $this->getUser()->getRoles()) or $this->getUser()->getArea()->getNombre() == 'SECRETARIA ADMINISTRATIVA'){
            $formFiltro = $this->createForm(FiltroSecAdminType::class);
        }else{
            $formFiltro = $this->createForm(FiltroType::class);
        }

        if ($request->query->get($formFiltro->getName())) {
            $formFiltro->handleRequest($request);
        }

        // Creo el $queryBuilder a trevés del cual se proporcionaran las entidades para
        // ser listadas. Como parametro paso el formulario con los filtros seleccionados
        $queryBuilder = $expedienteRepository->findForActionIndex($formFiltro->getData());
        $pagination = $paginator->paginate(
          $queryBuilder,
          $request->query->get('page', 1),
          12
        );     
        

        return $this->render('expediente/index.html.twig', [
          'pagination' => $pagination,
          'formFiltro' => $formFiltro->createView(),
        ]);
    }

    /**
     * @Route("/new", name="expediente_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $expediente = new Expediente();
        $form = $this->createForm(ExpedienteType::class, $expediente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            // Agregamos el usuario creador del expediente 
            $expediente->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($expediente);
            $entityManager->flush();
            // Creamos el primer movimiento del expediente
            $mov = new Movimiento();
            $mov->setExpediente($expediente);
            //$area = $entityManager->getRepository(Area::class)->find(6);
            $mov->setArea($this->getUser()->getArea()); 
            $mov->setFechaIngreso(new \DateTime());
            $mov->setEstado('En Revisión');
            $mov->setUbicacion(1);
            $mov->setUser($this->getUser());
            $entityManager->persist($mov);
            $entityManager->flush();

            // genaramos caratula en pdf
            
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');
            $pdfOptions->setIsRemoteEnabled(true);
            // Instancia Dompdf con opciones
            $dompdf = new Dompdf($pdfOptions);       

            // Recuperar el HTML generado en nuestro archivo twig
            $html = $this->renderView('expediente/print/expediente_print_caratula.html.twig', [
                'title' => "Caratula de Expediente",
                'expediente' => $expediente,
            ]);
            $html .= '<link type="text/css" href="../assets/templates/templates.css" rel="stylesheet" />';
            $nombrepdf="Expediente_N_".$expediente->getNumero();             

            // Cargar HTML a Dompdf
            $dompdf->loadHtml($html);
            
            // (Optional) Configure el tamaño del papel y la orientación 'portrait' or 'portrait'
            $dompdf->setPaper('A4', 'portrait');

            // Renderizar el HTML como PDF
            $dompdf->render();
            
            // Almacenar datos binarios PDF
            $output = $dompdf->output();
            
            // Preguntamos si existe el directorio publico, si no existe lo creamos
            $publicDirectory = $this->getParameter('document_directory');
            if(!is_dir($publicDirectory)){
                mkdir($publicDirectory);
            }

            // En este caso, queremos escribir el archivo en el directorio público.
            $pdfFilepath =  $publicDirectory . $nombrepdf;
            
            // Escriba el archivo en la ruta deseada
            file_put_contents($pdfFilepath, $output);


            $this->addFlash('success', '¡Registro agregado correctamente!');
            return $this->redirectToRoute('expediente_show',['id' => $expediente->getId()]);
        }

        return $this->render('expediente/new.html.twig', [
            'expediente' => $expediente,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="expediente_show", methods={"GET"})
     */
    public function show(Expediente $expediente, Request $request): Response
    {   
        $idArea = $request->query->get('area');
        
        return $this->render('expediente/show.html.twig', [
            'expediente' => $expediente,
            'area' => $idArea,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="expediente_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Expediente $expediente): Response
    {
        $form = $this->createForm(ExpedienteType::class, $expediente);
        $form->handleRequest($request);

        // Comprobamos si es el usuario creador del expediente 
        // solo el usuario creador puede editar el expediente
        if($expediente->getUser()->getId() == $this->getUser()->getId()){
            if ($form->isSubmitted() && $form->isValid()) {
            
                $this->getDoctrine()->getManager()->flush();
    
                $this->addFlash('success', '¡Registro actualizado correctamente!');

                // borramos pase viejo
                $routeFile = $this->getParameter('document_directory')."Expediente_N_".$expediente->getId();           
                if(file_exists($routeFile)){
                    unlink($routeFile);
                }

                // creamos caratula nueva en pdf
                $pdfOptions = new Options();
                $pdfOptions->set('defaultFont', 'Arial');
                $pdfOptions->setIsRemoteEnabled(true);
                // Instantiate Dompdf with our options
                $dompdf = new Dompdf($pdfOptions);       
                $html = $this->renderView('expediente/print/expediente_print_caratula.html.twig', [
                    'title' => "Caratula de Expediente",
                    'expediente' => $expediente
                ]);
                $html .= '<link type="text/css" href="../assets/templates/templates.css" rel="stylesheet" />';
                $nombrepdf="Expediente_N_".$expediente->getNumero();
                $dompdf->loadHtml($html);
                $dompdf->setPaper('A4', 'portrait');
                $dompdf->render();
                $output = $dompdf->output();
                $publicDirectory = $this->getParameter('document_directory');
                $pdfFilepath =  $publicDirectory . $nombrepdf;
                file_put_contents($pdfFilepath, $output);

                return $this->redirectToRoute('expediente_show', [
                    'id' => $expediente->getId(),
                ]);
            }
        }else{
            $this->addFlash('danger', '¡Usted no es el dueño de este Expediente, no lo podra editar!');
            return $this->redirectToRoute('expediente_index', [
                'id' => $expediente->getId(),
            ]);
        }        

        return $this->render('expediente/edit.html.twig', [
            'expediente' => $expediente,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="expediente_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Expediente $expediente): Response
    {
        if ($this->isCsrfTokenValid('delete'.$expediente->getId(), $request->request->get('_token'))) {
            $routeFile = $this->getParameter('document_directory')."Expediente_N_".$expediente->getId();           
            if(file_exists($routeFile)){
                unlink($routeFile);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($expediente);
            $entityManager->flush();
            
            $this->addFlash('success', '¡Registro eliminado correctamente!');
        }

        return $this->redirectToRoute('expediente_index');
    }

/***************************** ESTADISTICAS *****************************/
    /**
     * @Route("/estadisticas/principal/", name="expediente_estadisticas", methods={"GET"})
     */
    public function estadisticas(Request $request, ExpedienteRepository $expedienteRepository, PaginatorInterface $paginator): Response
    {
        $filter = "";
        $formFiltro = $this->createForm(FiltroEstadisticasType::class);
        if ($request->query->get($formFiltro->getName())) {
            $formFiltro->handleRequest($request);
            $filter = (array) $formFiltro->getData();
            if($formFiltro->getData()['juzgado']){
                $filter['juzgado'] = $formFiltro->getData()['juzgado']->getId();
            }
            if($formFiltro->getData()['area']){
                $filter['area'] = $formFiltro->getData()['area']->getId();
            }
            
        }

        // Creo el $queryBuilder a trevés del cual se proporcionaran las entidades para
        // ser listadas. Como parametro paso el formulario con los filtros seleccionados
        $queryBuilder = $expedienteRepository->findForEstadisticas($formFiltro->getData());
        $pagination = $paginator->paginate(
          $queryBuilder,
          $request->query->get('page', 1),
          12
        );     
        

        return $this->render('expediente/estadisticas.html.twig', [
          'pagination' => $pagination,
          'formFiltro' => $formFiltro->createView(),
          'filter' => $filter,
        ]);
    }



/***************************** PDF *****************************/

    /**
     * @Route("/print_caratula_expediente/{id}", name="expediente_print_caratula", methods={"GET"})
     */
    public function print_caratula_expediente(Request $request,Expediente $expediente) : Response
    {        
        $numero = $expediente->getNumero();

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);
        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);       

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('expediente/print/expediente_print_caratula.html.twig', [
            'title' => "Caratula de Expediente",
            'expediente' => $expediente
        ]);
        $html .= '<link type="text/css" href="../assets/templates/templates.css" rel="stylesheet" />';
        $nombrepdf="Expediente_".$numero.".pdf";   

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (inline view)
            
        $dompdf->stream($nombrepdf.".pdf", [
            "Attachment" => false
        ]);
        /*
        return $this->render('expedientes/expedinete_print_caratula.html.twig', [
                'expediente' => $expediente,
                'form' => $form->createView(),
            ]);
        */
    }

    /**
     * @Route("/print_pase_expediente/{id}/{pase}", name="expediente_print_pase", methods={"GET"})
     */
    public function print_pase_expediente(Request $request,Expediente $expediente) : Response
    {
      
        //$exp = $expediente->getId('id');
        $idPase = $request->get('pase');
        
        //$em = $this->getDoctrine()->getManager();
        //$expediente = $em->getRepository(Expediente::class)->find($exp);
        $em = $this->getDoctrine()->getManager();
        
        $pase = $em->getRepository(Movimiento::class)->find($idPase);
        $numero = $pase->getId();

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);
        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);       

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('expediente/print/expediente_print_pase.html.twig', [
            'title' => "Caratula de Expediente",
            'expediente' => $expediente,
            'pase' => $pase
        ]);
        $html .= '<link type="text/css" href="../assets/templates/templates.css" rel="stylesheet" />';
        $nombrepdf="Pase_N_".$numero;   

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();       

        // Output the generated PDF to Browser (inline view)
            
        $dompdf->stream($nombrepdf.".pdf", [
            "Attachment" => false
        ]);
        /*
        return $this->render('expedientes/expedinete_print_caratula.html.twig', [
                'expediente' => $expediente,
                'form' => $form->createView(),
            ]);
        */
    }

    /**
     * @Route("/print_expediente/{id}", name="expediente_print", methods={"GET"})
     */
    public function print_expediente(Request $request, Expediente $expediente) : Response
    {
        $route_exp = $this->getParameter('document_directory');
        $route_pase = $this->getParameter('pases_directory');
        $route_mov = $this->getParameter('movimientos_directory');
        
        # Rutas de los documentos
        //$documentos = ["cotizacion.pdf", "parzibyte.pdf", "documento.pdf"];
        $documentos = [];
        $documentos[] = $route_exp."Expediente_N_".$expediente->getNumero();
        if($expediente->getArchivo()){
            $documentos[] = $route_exp.$expediente->getArchivo();
        }
        $i=0;
        foreach ($expediente->getMovimientos() as $m){
            if($i != 0){
                $documentos[] = $route_pase."Pase_N_".$m->getId();
                if($m->getArchivo()){
                    $documentos[] = $route_mov.$m->getArchivo();
                }
            }    
            $i++;
        }
        
        # Crear el "combinador"
        $combinador = new Merger;

        //$combinador->addFile($route_mov.'/REDES-4-15-03-2022.pdf', new Pages('1-2'));
        # Agregar archivo en cada iteración
        foreach ($documentos as $documento) {
            $combinador->addFile($documento);
        }

        # Y combinar o unir
        $salida = $combinador->merge();

        /*
        Ahora la salida la mostramos directamente en la petición,
        y enviamos unos encabezados para que el navegador
        lo interprete
        */
        # Este nombre se pondrá como título o nombre del documento
        $nombreArchivo = $expediente->getNumero().".pdf";

        # Escribir salida en el nombre del archivo        
        header("Content-type:application/pdf");
        header('Content-disposition: inline; filename="$nombreArchivo"');
        header("content-Transfer-Encoding:binary");
        header("Accept-Ranges:bytes");
        
        # Imprimir salida luego de encabezados
        echo $salida;

        /*
            Aquí puedes hacer más cosas pero asegúrate
            de no imprimir absolutamente nada; en este caso
            pongo exit para terminar el script inmediatamente
        */
        exit;
        //var_dump('si');
        //die;
    }   

    /**
     * @Route("/print_expediente_movimientos/{id}", name="expediente_print_movimientos", methods={"GET"})
     */
    public function print_movimientos_expediente(Request $request,Expediente $expediente) : Response
    {        
        $numero = $expediente->getNumero();

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);
        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);       

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('expediente/print/expediente_mivimientos.html.twig', [
            'title' => "Caratula de Expediente",
            'expediente' => $expediente
        ]);
        $html .= '<link type="text/css" href="../assets/templates/templates.css" rel="stylesheet" />';
        $nombrepdf="Exp_".$numero."_movimientos.pdf";   

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (inline view)
            
        $dompdf->stream($nombrepdf.".pdf", [
            "Attachment" => false
        ]);
        /*
        return $this->render('expedientes/expedinete_print_caratula.html.twig', [
                'expediente' => $expediente,
                'form' => $form->createView(),
            ]);
        */
    }

    /**
     * @Route("/print_expediente_estadisticas/{?filter}", name="expediente_print_estadisticas", methods={"GET"})
     */
    public function print_estadistica_expediente(Request $request, ExpedienteRepository $expedienteRepository) : Response
    {        
        $filtro = $request->get('filter');
        if(!$filtro){
            $filtro = [];
        }
        $expedientes = $expedienteRepository->findForEstadisticas($filtro);
/*
        //$queryBuilder = $expedienteRepository->findForEstadisticas($filtro);
        var_dump($filtro);
        echo "<br> ";
        if($expedientes){
            echo "si <br>";
            foreach($expedientes as $e){
                echo "si". $e->getId();
            }
        }else{
            echo "no";
        }
        //var_dump($queryBuilder);
        die;

  */      // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);
        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);       

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('expediente/print/expediente_estadisticas.html.twig', [
            'title' => "Caratula de Expediente",
            'expedientes' => $expedientes,
        ]);
        $html .= '<link type="text/css" href="../assets/templates/templates.css" rel="stylesheet" />';
        $nombrepdf="Expedientes_buscados.pdf";   

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (inline view)
            
        $dompdf->stream($nombrepdf.".pdf", [
            "Attachment" => false
        ]);
        /*
        return $this->render('expedientes/expedinete_print_caratula.html.twig', [
                'expediente' => $expediente,
                'form' => $form->createView(),
            ]);
        */
    }

}
