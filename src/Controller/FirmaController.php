<?php

namespace App\Controller;

use App\Entity\Firma;
use App\Entity\Movimiento;
use App\Entity\Persona;
use App\Form\FirmaType;
use App\Form\Firma\FiltroType;
use App\Form\FirmaArchivoType;
use App\Repository\FirmaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/admin/firma")
 */
class FirmaController extends AbstractController
{
    /**
     * @Route("/", name="firma_index", methods={"GET"})
     */
    public function index(Request $request, FirmaRepository $firmaRepository, PaginatorInterface $paginator): Response
    {
        if(in_array("ROLE_SUPER_ADMIN", $this->getUser()->getRoles()) or $this->getUser()->getArea()->getNombre() == 'SECRETARIA ADMINISTRATIVA'){
            $formFiltro = $this->createForm(FiltroType::class);
        }else{
            $formFiltro = $this->createForm(FiltroType::class);
        }

        if ($request->query->get($formFiltro->getName())) {
            $formFiltro->handleRequest($request);
        }

        $queryBuilder = $firmaRepository->findForActionIndex($formFiltro->getData());
        $pagination = $paginator->paginate(
          $queryBuilder,
          $request->query->get('page', 1),
          12
        );

        return $this->render('firma/index.html.twig', [
          'pagination' => $pagination,
          'formFiltro' => $formFiltro->createView(),
        ]);
    }

    /**
     * @Route("/new/{id}", name="persona_firma_new", methods={"GET","POST"})
     */
    public function newFirmaPersona(Request $request, Persona $persona, FirmaRepository $repositoryFrima): Response
    {   
        $firma = new Firma();
        $firma->setPersona($persona);

        $form = $this->createForm(FirmaType::class, $firma);
        $form->handleRequest($request);   

        if ($form->isSubmitted() && $form->isValid()) {            
            $data['persona'] = $persona->getId();
            $data['area'] = $firma->getArea()->getId();         
            
            $exit = $repositoryFrima->findPersonaArea($data);
            if($exit){
                $this->addFlash('danger', '¡'.$firma->getArea()->getNombre().' ya tiene autorizacion de firma!');
                return $this->redirectToRoute('persona_firma_new',['id' => $persona->getId()]);
            }else{
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($firma);
                $entityManager->flush();
                
                $this->addFlash('success', '¡Registro agregado!');
                
                return $this->redirectToRoute('persona_show',['id' => $persona->getId()]);
            }die;
            
        }

        return $this->render('firma/new.html.twig', [
            'firma' => $firma,
            'form' => $form->createView(),
            'idPer' => $persona->getId(),
        ]);
    }    

    /**
     * @Route("/new/{id?}", name="firma_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $idPer = $request->get('id');
        $firma = new Firma();
        if($idPer){
            $entityManager = $this->getDoctrine()->getManager();
            $per = $entityManager->getRepository(Persona::class)->find($idPer);
            $firma->setPersona($per);     
        }
        
        $form = $this->createForm(FirmaType::class, $firma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($firma);
            $entityManager->flush();
            
            $this->addFlash('success', '¡Registro agregado correctamente!');
            
            return $this->redirectToRoute('persona_show',['id'=>$firma->getPersona()->getId()]);
        }

        return $this->render('firma/new.html.twig', [
            'firma' => $firma,
            'form' => $form->createView(),
            'idPer' => $idPer,
        ]);
    }

    /**
     * @Route("/{id}", name="firma_show", methods={"GET"})
     */
    public function show(Firma $firma): Response
    {
        return $this->render('firma/show.html.twig', [
            'firma' => $firma,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="firma_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Firma $firma): Response
    {
        $form = $this->createForm(FirmaType::class, $firma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', '¡Registro actualizado correctamente!');
            return $this->redirectToRoute('firma_index', [
                'id' => $firma->getId(),
            ]);
        }

        return $this->render('firma/edit.html.twig', [
            'firma' => $firma,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="firma_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Firma $firma): Response
    {
        if ($this->isCsrfTokenValid('delete'.$firma->getId(), $request->request->get('_token'))) {
            // borramos archivo
            $routeFile = $this->getParameter('firmas_directory').$firma->getArchivo();               
            if(file_exists($routeFile)){
                unlink($routeFile);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($firma);
            $entityManager->flush();
            $this->addFlash('success', '¡Registro eliminado correctamente!');
        }

        return $this->redirectToRoute('firma_index');
    }

     /**
     * @Route("/archivo/new/{id}", name="firma_archivo_new", methods={"GET","POST"})
     */
    public function newArchivo(Request $request, Persona $persona)
    {   

        $form = $this->createForm(FirmaArchivoType::class,[]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           
            /** @var UploadedFile $brochureFile */
            $brochureFile = $form['archivo']->getData();
            
            // esta condición es necesaria porque el campo 'brochure' no es obligatorio
            // por lo tanto, el archivo PDF debe procesarse solo cuando se carga un archivo 
            if ($brochureFile && ($brochureFile->guessExtension() == 'png' || $brochureFile->guessExtension() == 'jpg')) {
                // esto es necesario para incluir de forma segura el nombre del archivo como parte de la URL 
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = "firma_".$persona->getDni().'.'.$brochureFile->guessExtension();
               
                // Mueva el archivo al directorio donde se almacenan 'brochures'
                try {
                    $brochureFile->move(
                        $this->getParameter('firmas_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    //... manejar la excepción si algo sucede durante la carga del archivo 
                }
                
                $persona->setFirma($newFilename);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($persona);
                $entityManager->flush();
            }else{
                $this->addFlash('danger', '¡El formato de la imgen no es valido!');
                return $this->render('firma/newArchivo.html.twig', [
                    'persona' => $persona->getId(),
                    'form' => $form->createView(),
                ]);
            }

            $this->addFlash('success', '¡Registro agregado correctamente!');
            
            return $this->redirectToRoute('persona_show',['id'=>$persona->getId()]);
        }

        return $this->render('firma/newArchivo.html.twig', [
            'persona' => $persona->getId(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/archivo/{id}", name="firma_archivo_delete", methods={"GET","DELETE"})
     */
    public function deleteArchivo(Request $request, Persona $persona): Response
    {
        if ($this->isCsrfTokenValid('delete'.$persona->getId(), $request->request->get('_token'))) {
            
            // borramos archivo
            $routeFile = $this->getParameter('firmas_directory').$persona->getFirma();               
            if(file_exists($routeFile)){
                unlink($routeFile);
            }

            $persona->setFirma("");
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($persona);
            $entityManager->flush();
            $this->addFlash('success', '¡El archivo fue eliminado correctamente!');
        }

        return $this->redirectToRoute('persona_show',['id' => $persona->getId()]);
    }

    /**
     * @Route("/pase/{id}", name="firma_pase", methods={"GET"})
     */
    public function firmaPase(Request $request, Movimiento $movimiento): Response
    {
        $idFir = $request->get('idF');

        $routeFile = $this->getParameter('pases_directory')."Pase_N_".$movimiento->getId();               
        if(file_exists($routeFile)){
            unlink($routeFile);
        }

        $em = $this->getDoctrine()->getManager();
        $firma = $em->getRepository(Firma::class)->find($idFir);
        $routeFile = '../../public/uploads/firmas/';
        $firmaFile = $routeFile.$firma->getArchivo();
        $numero = $movimiento->getId();

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);
        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);       

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('expediente/caratula/expediente_print_pase.html.twig', [
            'title' => "Caratula de Expediente",
            'expediente' => $movimiento->getExpediente(),
            'pase' => $movimiento,
            'firma' => $firmaFile,
        ]);
        $html .= '<link type="text/css" href="../assets/templates/templates.css" rel="stylesheet" />';
        $nombrepdf="Pase_N_".$numero;   

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);
        
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();       
        // Almacenar datos binarios PDF
        $output = $dompdf->output();
                
        // En este caso, queremos escribir el archivo en el directorio público.
        $publicDirectory = $this->getParameter('pases_directory');
        $pdfFilepath =  $publicDirectory . $nombrepdf;
        
        // Escriba el archivo en la ruta deseada
        file_put_contents($pdfFilepath, $output);
        // Output the generated PDF to Browser (inline view)
        /*    
        $dompdf->stream($nombrepdf.".pdf", [
            "Attachment" => false
        ]);
        /*
        return $this->render('expedientes/expedinete_print_caratula.html.twig', [
                'expediente' => $expediente,
                'form' => $form->createView(),
            ]);
        */
    }
}
