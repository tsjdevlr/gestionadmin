<?php

namespace App\Controller;

use App\Entity\Expediente;
use App\Entity\Movimiento;
use App\Form\ArchivoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ArchivoController extends AbstractController
{
    /**
     * @Route("/admin/documento/new", name="document_new", methods={"GET","POST"})
     */
    public function new(Request $request)
    {   
        $exp = $request->get('exp');
        $form = $this->createForm(ArchivoType::class,[]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $brochureFile */
            $brochureFile = $form['archivo']->getData();

            // esta condición es necesaria porque el campo 'brochure' no es obligatorio
            // por lo tanto, el archivo PDF debe procesarse solo cuando se carga un archivo 
            if ($brochureFile) {
                // esto es necesario para incluir de forma segura el nombre del archivo como parte de la URL 
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
                
                $entityManager = $this->getDoctrine()->getManager();
                $expediente = $entityManager->getRepository(Expediente::class)->find($exp);
                $this->addFlash('success', '¡Archivo agregado correctamente!');
                $newFilename = $expediente->getNumero().'.'.$brochureFile->guessExtension();
               
                // Mueva el archivo al directorio donde se almacenan 'brochures'
                try {
                    $brochureFile->move(
                        $this->getParameter('document_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    //... manejar la excepción si algo sucede durante la carga del archivo 
                }

                $expediente->setArchivo($newFilename);
                $entityManager->persist($expediente);
                $entityManager->flush();
    
                return $this->redirect($this->generateUrl('expediente_show', ['id' => $exp]));
            }           
           
        }

        return $this->render('expediente/archivo.html.twig', [
            'form' => $form->createView(),
            'id' => $exp,
        ]);
    }

    /**
     * @Route("/admin/documento/delete", name="document_exp_delete", methods={"DELETE"})
     */
    public function deletePedido(Request $request){

        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $expediente = $em->getRepository(Expediente::class)->find($id);

        if ($this->isCsrfTokenValid('delete'.$expediente->getId(), $request->request->get('_token'))) {

            // Eliminamos el archivo del expediente
            $routeFile = $this->getParameter('document_directory')."/".$expediente->getArchivo();               
            unlink($routeFile);
            $expediente->setArchivo(null);
            $em->persist($expediente);
            $em->flush();

            $this->addFlash('success', '¡Archivo eliminado correctamente!');
        }

        return $this->redirect($this->generateUrl('expediente_show', ['id' => $id]));

    }

    /**
     * @Route("/admin/documento/mov/new", name="document_mov_new", methods={"GET","POST"})
     */
    public function newMovimiento(Request $request)
    {   
        $exp = $request->get('exp');
        $mov = $request->get('mov');
        $form = $this->createForm(ArchivoType::class,[]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $brochureFile */
            $brochureFile = $form['archivo']->getData();

            // esta condición es necesaria porque el campo 'brochure' no es obligatorio
            // por lo tanto, el archivo PDF debe procesarse solo cuando se carga un archivo 
            if ($brochureFile) {
                // esto es necesario para incluir de forma segura el nombre del archivo como parte de la URL 
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
                
                $entityManager = $this->getDoctrine()->getManager();
                $expediente = $entityManager->getRepository(Expediente::class)->find($exp);
                $movimiento = $entityManager->getRepository(Movimiento::class)->find($mov);
                $this->addFlash('success', '¡Archivo agregado correctamente!');
                // nombre del archivo (newFilename) formado por el N° Expte|Area que proviene |Fecha del movimiento
                $fecha = new \DateTime();
                $fecha = $fecha->format('d-m-Y');
                
                $newFilename = $movimiento->getUser()->getArea()->getNombre().'-'.$movimiento->getId().'-'.$fecha.'.'.$brochureFile->guessExtension();
               
                // Mueva el archivo al directorio donde se almacenan 'brochures'
                try {
                    $brochureFile->move(
                        $this->getParameter('movimientos_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    //... manejar la excepción si algo sucede durante la carga del archivo 
                }

                $movimiento->setArchivo($newFilename);
                $entityManager->persist($movimiento);
                $entityManager->flush();
    
                return $this->redirect($this->generateUrl('movimiento_show', ['id' => $mov, 'exp' => $exp]));
            }           
           
        }

        return $this->render('movimiento/archivo.html.twig', [
            'form' => $form->createView(),
            'id' => $exp,
            'idSeg' => $mov,
        ]);
    }

    /**
     * @Route("/admin/documento/mov/delete", name="document_mov_delete", methods={"DELETE"})
     */
    public function deleteMovimiento(Request $request){

        $id = $request->get('id');
        $exp = $request->get('exp');
        $em = $this->getDoctrine()->getManager();

        $movimiento = $em->getRepository(Movimiento::class)->find($id);

        if ($this->isCsrfTokenValid('delete'.$movimiento->getId(), $request->request->get('_token'))) {

            // Eliminamos el archivo del movimiento
            $routeFile = $this->getParameter('movimientos_directory')."/".$movimiento->getArchivo();               
            unlink($routeFile);
            $movimiento->setArchivo(null);
            $em->persist($movimiento);
            $em->flush();

            $this->addFlash('success', '¡Archivo eliminado correctamente!');
        }

        return $this->redirect($this->generateUrl('movimiento_show', ['id' => $id, 'exp' => $exp]));

    }
}
