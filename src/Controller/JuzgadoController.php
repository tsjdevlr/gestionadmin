<?php

namespace App\Controller;

use App\Entity\Juzgado;
use App\Form\JuzgadoType;
use App\Form\Juzgado\FiltroType;
use App\Repository\JuzgadoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("admin/juzgado")
 */
class JuzgadoController extends AbstractController
{
    /**
     * @Route("/", name="juzgado_index", methods={"GET"})
     */
    public function index(Request $request, JuzgadoRepository $juzgadoRepository, PaginatorInterface $paginator): Response
    {
        // Creo el formulario del filtro.
        // Luego consulto si viene el formulario en el $request para
        // asociar esos valores en el formulario
        $formFiltro = $this->createForm(FiltroType::class);
        if ($request->query->get($formFiltro->getName())) {
            $formFiltro->handleRequest($request);
        }

        // Creo el $queryBuilder a trevés del cual se proporcionaran las entidades para
        // ser listadas. Como parametro paso el formulario con los filtros seleccionados
        $queryBuilder = $juzgadoRepository->findForActionIndex($formFiltro->getData());
        $pagination = $paginator->paginate(
          $queryBuilder,
          $request->query->get('page', 1),
          12
        );

        return $this->render('juzgado/index.html.twig', [
          'pagination' => $pagination,
          'formFiltro' => $formFiltro->createView(),
        ]);
    }

    /**
     * @Route("/new", name="juzgado_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $juzgado = new Juzgado();
        $form = $this->createForm(JuzgadoType::class, $juzgado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($juzgado);
            $entityManager->flush();

            $this->addFlash('success', '¡Registro agregado correctamente!');
            return $this->redirectToRoute('juzgado_index');
        }

        return $this->render('juzgado/new.html.twig', [
            'juzgado' => $juzgado,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="juzgado_show", methods={"GET"})
     */
    public function show(Juzgado $juzgado): Response
    {
        return $this->render('juzgado/show.html.twig', [
            'juzgado' => $juzgado,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="juzgado_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Juzgado $juzgado): Response
    {
        $form = $this->createForm(JuzgadoType::class, $juzgado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', '¡Registro actualizado correctamente!');
            return $this->redirectToRoute('juzgado_index', [
                'id' => $juzgado->getId(),
            ]);
        }

        return $this->render('juzgado/edit.html.twig', [
            'juzgado' => $juzgado,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="juzgado_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Juzgado $juzgado): Response
    {
        if ($this->isCsrfTokenValid('delete'.$juzgado->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($juzgado);
            $entityManager->flush();
            $this->addFlash('success', '¡Registro eliminado correctamente!');
        }

        return $this->redirectToRoute('juzgado_index');
    }
}
