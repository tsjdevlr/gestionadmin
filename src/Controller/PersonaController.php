<?php

namespace App\Controller;

use App\Entity\Persona;
use App\Entity\Expediente;
use App\Form\PersonaType;
use App\Form\Persona\FiltroType;
use App\Repository\PersonaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/admin/persona")
 */
class PersonaController extends AbstractController
{
    /**
     * @Route("/", name="persona_index", methods={"GET"})
     */
    public function index(Request $request, PersonaRepository $personaRepository, PaginatorInterface $paginator): Response
    {
        // Creo el formulario del filtro.
        // Luego consulto si viene el formulario en el $request para
        // asociar esos valores en el formulario
        $formFiltro = $this->createForm(FiltroType::class);
        if ($request->query->get($formFiltro->getName())) {
            $formFiltro->handleRequest($request);
        }

        // Creo el $queryBuilder a trevés del cual se proporcionaran las entidades para
        // ser listadas. Como parametro paso el formulario con los filtros seleccionados
        $queryBuilder = $personaRepository->findForActionIndex($formFiltro->getData());
        $pagination = $paginator->paginate(
          $queryBuilder,
          $request->query->get('page', 1),
          12
        );

        return $this->render('persona/index.html.twig', [
          'pagination' => $pagination,
          'formFiltro' => $formFiltro->createView(),
        ]);
    }

    /**
     * @Route("/new/{exp?}/", name="persona_new", methods={"GET","POST"})
     */
    public function new(Request $request, PersonaRepository $personaRepository, PaginatorInterface $paginator): Response
    {   
        $exp = $request->get('exp');
        if($exp){
            $em = $this->getDoctrine()->getManager();
            $expediente = $em->getRepository(Expediente::class)->find($exp);
        }
        
        $persona = new Persona();
        $form = $this->createForm(PersonaType::class, $persona);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($persona);
            $entityManager->flush();            
            
            if($exp){
                $expediente->setSolicitante($persona);
                $em->persist($expediente);
                $em->flush();
                $this->addFlash('success', '¡Solicitante agregado correctamente!');
                return $this->redirectToRoute('expediente_show',['id' => $exp]);
            }else{
                $this->addFlash('success', '¡Persona agregada correctamente!');
                return $this->redirectToRoute('persona_show',['id' => $persona->getId()]);
            }
            
            
        }

        $formFiltro = $this->createForm(FiltroType::class);
        
        if ($request->query->get($formFiltro->getName())) {
            $formFiltro->handleRequest($request);
        }
        $queryBuilder = $personaRepository->findForActionIndex($formFiltro->getData());
        $pagination = $paginator->paginate(
          $queryBuilder,
          $request->query->get('page', 1),
          3
        );
        if($exp){
            return $this->render('persona/new.html.twig', [
                'persona' => $persona,
                'form' => $form->createView(),
                'exp' => $expediente,
                'pagination' => $pagination,
                'formFiltro' => $formFiltro->createView(),
            ]);    
        }
        return $this->render('persona/newPersona.html.twig', [
            'form' => $form->createView(),
            'pagination' => $pagination,
            'exp' => $exp,
        ]);
    }

    /**
     * @Route("/solicitante", name="solicitabte_new", methods={"GET","POST"})
     */
    public function newSolicitante(Request $request){
        $exp = $request->query->get('exp');
        $per = $request->query->get('per');

        $entityManager = $this->getDoctrine()->getManager();
        $persona = $entityManager->getRepository(Persona::class)->find($per);
            
        $em = $this->getDoctrine()->getManager();
        $expediente = $em->getRepository(Expediente::class)->find($exp);
        $expediente->setSolicitante($persona);
        $em->persist($expediente);
        $em->flush();

        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);
        // Instancia Dompdf con opciones
        $dompdf = new Dompdf($pdfOptions);       

        // Recuperar el HTML generado en nuestro archivo twig
        $html = $this->renderView('expediente/print/expediente_print_caratula.html.twig', [
            'title' => "Caratula de Expediente",
            'expediente' => $expediente,
        ]);
        $html .= '<link type="text/css" href="../assets/templates/templates.css" rel="stylesheet" />';
        $nombrepdf="Expediente_N_".$expediente->getNumero();             

        // Cargar HTML a Dompdf
        $dompdf->loadHtml($html);
        
        // (Optional) Configure el tamaño del papel y la orientación 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Renderizar el HTML como PDF
        $dompdf->render();
        
        // Almacenar datos binarios PDF
        $output = $dompdf->output();
        
        // Preguntamos si existe el directorio publico, si no existe lo creamos
        $publicDirectory = $this->getParameter('document_directory');
        if(!is_dir($publicDirectory)){
            mkdir($publicDirectory);
        }

        // En este caso, queremos escribir el archivo en el directorio público.
        $pdfFilepath =  $publicDirectory . $nombrepdf;
        
        if(file_exists($pdfFilepath)){
            unlink($pdfFilepath);
        }
        // Escriba el archivo en la ruta deseada
        file_put_contents($pdfFilepath, $output);


        $this->addFlash('success', '¡Registro agregado correctamente!');
        return $this->redirectToRoute('expediente_show',['id' => $expediente->getId()]);    

    }

    /**
     * @Route("/solicitanteEdit", name="solicitante_edit", methods={"GET","POST"})
     */
    public function editSolicitante(Request $request): Response
    {
        $form = $this->createForm(PersonaType::class, $persona);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', '¡Registro actualizado correctamente!');
            return $this->redirectToRoute('persona_index', [
                'id' => $persona->getId(),
            ]);
        }

        return $this->render('persona/edit.html.twig', [
            'persona' => $persona,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="persona_show", methods={"GET"})
     */
    public function show(Persona $persona, Request $request): Response
    {
        $exp = $request->get('exp');
        return $this->render('persona/show.html.twig', [
            'persona' => $persona,
            'exp' => $exp,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="persona_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Persona $persona): Response
    {
        $exp = $request->get('exp');
        $form = $this->createForm(PersonaType::class, $persona);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', '¡Registro actualizado correctamente!');
            if($exp){
                return $this->redirectToRoute('persona_show', [
                    'id' => $persona->getId(),
                    'exp' => $exp,
                ]);
            }else{
                return $this->redirectToRoute('persona_show', [
                    'id' => $persona->getId(),
                ]);
            }
           
        }

        return $this->render('persona/edit.html.twig', [
            'persona' => $persona,
            'form' => $form->createView(),
            'exp' => $exp,
        ]);
    }

    /**
     * @Route("/{id}", name="persona_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Persona $persona): Response
    {
        if ($this->isCsrfTokenValid('delete'.$persona->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($persona);
            $entityManager->flush();
            $this->addFlash('success', '¡Registro eliminado correctamente!');
        }

        return $this->redirectToRoute('persona_index');
    }
}
