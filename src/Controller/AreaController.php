<?php

namespace App\Controller;

use App\Entity\Area;
use App\Form\AreaType;
use App\Form\AreaDependienteType;
use App\Form\AreaEditType;
use App\Form\Area\FiltroType;
use App\Repository\AreaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("(/admin/area")
 */
class AreaController extends AbstractController
{
    /**
     * @Route("/", name="area_index", methods={"GET"})
     */
    public function index(Request $request, AreaRepository $areaRepository, PaginatorInterface $paginator): Response
    {
        // Creo el formulario del filtro.
        // Luego consulto si viene el formulario en el $request para
        // asociar esos valores en el formulario
        $formFiltro = $this->createForm(FiltroType::class);
        if ($request->query->get($formFiltro->getName())) {
            $formFiltro->handleRequest($request);
        }

        // Creo el $queryBuilder a trevés del cual se proporcionaran las entidades para
        // ser listadas. Como parametro paso el formulario con los filtros seleccionados
        $queryBuilder = $areaRepository->findForActionIndex($formFiltro->getData());
        $pagination = $paginator->paginate(
          $queryBuilder,
          $request->query->get('page', 1),
          120
        );

        return $this->render('area/index.html.twig', [
          'pagination' => $pagination,
          'formFiltro' => $formFiltro->createView(),
        ]);
    }

    /**
     * @Route("/new", name="area_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $area = new Area();
        $id = $request->query->get('id');
        $form = $this->createForm(AreaType::class, $area);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            if($id){
                $idArea = $this->getDoctrine()->getRepository(Area::class)->find($id);
                $area->setParent($idArea);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($area);
            $entityManager->flush();

            $this->addFlash('success', '¡Registro agregado correctamente!');
            if (in_array("ROLE_AREA_ADMIN", $this->getUser()->getRoles())) {
                return $this->redirectToRoute('area_show',['id' => $this->getUser()->getArea()->getId()]);
            }else{
                return $this->redirectToRoute('area_index');
            }
            
        }

        return $this->render('area/new.html.twig', [
            'area' => $area,
            'form' => $form->createView(),
            'id' => $id,
        ]);
    }

    /**
     * @Route("/{id}", name="area_show", methods={"GET"})
     */
    public function show(Area $area, Request $request): Response
    {
        
        $id = $request->query->get('area');
        return $this->render('area/show.html.twig', [
            'area' => $area,
            'id' => $id,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="area_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Area $area): Response
    {
        $form = $this->createForm(AreaEditType::class, $area);
        $form->handleRequest($request);
        $id = $request->query->get('area');
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            if($id){
                $this->addFlash('success', '¡Registro actualizado correctamente!');
                return $this->redirectToRoute('area_show', [
                    'id' => $id,
                ]);
            }else{
                $this->addFlash('success', '¡Registro actualizado correctamente!');
                return $this->redirectToRoute('area_show', [
                    'id' => $area->getId(),
                ]);
            }
        }
        if($id){
            return $this->render('area/edit.html.twig', [
                'area' => $area,
                'form' => $form->createView(),
                'id' => $id,
            ]);      
        }else{
            return $this->render('area/edit.html.twig', [
                'area' => $area,
                'form' => $form->createView(),
                'id' => '',
            ]);
        }
        
    }

    /**
     * @Route("/{id}", name="area_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Area $area): Response
    {
        if ($this->isCsrfTokenValid('delete'.$area->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($area);
            $entityManager->flush();
            $this->addFlash('success', '¡Registro eliminado correctamente!');
        }

        return $this->redirectToRoute('area_index');
    }

     /**
     * @Route("/{id}/{area}", name="area_dependiente_delete", methods={"DELETE","GET"})
     */
    public function deleteDepende(Request $request, Area $area): Response
    {
        $id = $request->query->get('area');
        var_dump($id);
        die();
        if ($this->isCsrfTokenValid('delete'.$area->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($area);
            $entityManager->flush();
            $this->addFlash('success', '¡Registro eliminado correctamente!');
        }

        return $this->redirectToRoute('area_show',['id'=>$id]);
    }
}
