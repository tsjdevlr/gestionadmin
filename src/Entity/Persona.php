<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table("Personas")
 * @ORM\Entity(repositoryClass="App\Repository\PersonaRepository")
 * @Gedmo\Loggable
 */
class Persona
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string", nullable=true, length=255)
     * @Gedmo\Versioned
     * @Assert\NotBlank(message="Por favor ingrese su nombre.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "El nombre es demasiado corto.",
     *      maxMessage = "El nombre es demasiado largo.",
     *      groups={"Registration", "Profile"}
     * )
     *
     */
    protected $nombre;

    /**
     * @var string $apellidos
     * @ORM\Column(name="apellidos", type="string", nullable=true, length=255)
     * @Gedmo\Versioned
     * @Assert\NotBlank(message="Por favor ingrese su apellido.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "El apellido es demasiado corto.",
     *      maxMessage = "El apellido es demasiado largo.",
     *      groups={"Registration", "Profile"}
     * )
     *
     */
    protected $lastName;

    /**
     * @var string $dni
     * @ORM\Column(name="dni", type="integer", nullable=true, length=12)
     * @Gedmo\Versioned
     * @Assert\NotBlank(message="Por favor ingrese su apellido.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      min = 10000000,
     *      max = 50000000,
     *      minMessage = "El dni es demasiado pequeño, no es valido.",
     *      maxMessage = "El dni es demasiado grande, no es valido",
     *      groups={"Registration", "Profile"}
     * )
     *
     */
    protected $dni;

     /**
     * @var string $telephone
     * @Gedmo\Versioned
     * @ORM\Column(name="telephone", type="string", nullable=true, length=255)
     */
    protected $telephone;

    /**
     * @var string $celular
     * @Gedmo\Versioned
     * @ORM\Column(name="cellphone", type="string", nullable=true, length=255)
     */
    protected $cellphone;

    /**
    * @var string
    *
    * @ORM\Column(name="firma", type="string", nullable=true)
    */
    protected $firma;


    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity=Expediente::class, mappedBy="solicitante")
     */
    private $expedientes;

    /**
     * @ORM\OneToMany(targetEntity=Firma::class, mappedBy="persona")
     */
    private $firmas;

    public function __construct()
    {
        $this->expedientes = new ArrayCollection();
        $this->firmas = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = strtoupper($lastName);

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(?string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getCellphone(): ?string
    {
        return $this->cellphone;
    }

    public function setCellphone(?string $cellphone): self
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|Expediente[]
     */
    public function getExpedientes(): Collection
    {
        return $this->expedientes;
    }

    public function addExpediente(Expediente $expediente): self
    {
        if (!$this->expedientes->contains($expediente)) {
            $this->expedientes[] = $expediente;
            $expediente->setSolicitante($this);
        }

        return $this;
    }

    public function removeExpediente(Expediente $expediente): self
    {
        if ($this->expedientes->removeElement($expediente)) {
            // set the owning side to null (unless already changed)
            if ($expediente->getSolicitante() === $this) {
                $expediente->setSolicitante(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Firma[]
     */
    public function getFirmas(): Collection
    {
        return $this->firmas;
    }

    public function addFirma(Firma $firma): self
    {
        if (!$this->firmas->contains($firma)) {
            $this->firmas[] = $firma;
            $firma->setPersona($this);
        }

        return $this;
    }

    public function removeFirma(Firma $firma): self
    {
        if ($this->firmas->removeElement($firma)) {
            // set the owning side to null (unless already changed)
            if ($firma->getPersona() === $this) {
                $firma->setPersona(null);
            }
        }

        return $this;
    }

    public function getFirma(): ?string
    {
        return $this->firma;
    }

    public function setFirma(?string $firma): self
    {
        $this->firma = $firma;

        return $this;
    }
}
