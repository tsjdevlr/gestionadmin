<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table("expedientes")
 * @ORM\Entity(repositoryClass="App\Repository\ExpedienteRepository")
 * @Gedmo\Loggable
 * @UniqueEntity("numero")
 */
class Expediente
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    
    /**
     * @var string $numero
     * @ORM\Column(name="numero", type="string", nullable=true, length=255)
     * @Gedmo\Versioned
     * @Assert\NotBlank(message="Por favor ingrese el numero.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "El numero es demasiado corto.",
     *      maxMessage = "El numero es demasiado largo.",
     *      groups={"Registration", "Profile"}
     * )
     *
     */
    
    protected $numero;

     /**
     * @var string $letra
     * @ORM\Column(name="letra", type="string", nullable=false, length=5)
     * @Gedmo\Versioned
     * @Assert\NotBlank(message="Por favor ingrese Letra.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      max = 1,
     *      maxMessage = "Solo se permite el ingreso de una Letra, solo una.",
     *      groups={"Registration", "Profile"}
     * )
     *
     */
    
    protected $letra;

    /**
     * @var string $caratula
     * @ORM\Column(name="caratula", type="string", nullable=true, length=255)
     * @Gedmo\Versioned
     * @Assert\NotBlank(message="Por favor ingrese Caratula.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "La caratula es demasiado larga.",
     *      groups={"Registration", "Profile"}
     * )
     *
     */
    
    protected $caratula;
    
    /**
     * @var string $fecha
     * @ORM\Column(name="fecha", type="date", nullable=true)
     * 
     */
    
    protected $fecha;

    /**
     * @var string $estado
     * @ORM\Column(name="estado", type="string", nullable=true, length=50)
     */
    
    protected $estado;

    /**
    * @var string
    *
    * @ORM\Column(name="archivo", type="string", nullable=true)
    */
    protected $archivo;

    /**
     * @ORM\ManyToOne(targetEntity=Juzgado::class, inversedBy="expedientes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $juzgado;

    /**
     * @ORM\ManyToOne(targetEntity=Area::class, inversedBy="expedientes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $area;

    /**
     * @ORM\OneToMany(targetEntity=Movimiento::class, mappedBy="Expediente", cascade={"remove"})
     */
    private $movimientos;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="expedientes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

     /**
     * @ORM\ManyToOne(targetEntity=Persona::class, inversedBy="expedientes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $solicitante;

    /**
      * @var \DateTime
      *
      * @ORM\Column(name="createdAt", type="datetime")
      * @Gedmo\Timestampable(on="create")
      */
      private $createdAt;

      /**
      * @var \DateTime
      *
      * @ORM\Column(name="updatedAt", type="datetime")
      * @Gedmo\Timestampable(on="update")
      * @Gedmo\Versioned
      */
    private $updatedAt;

    public function __construct()
    {
        $this->movimientos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getLetra(): ?string
    {
        return $this->letra;
    }

    public function setLetra(?string $letra): self
    {
        $this->letra = strtoupper($letra);

        return $this;
    }

    public function getCaratula(): ?string
    {
        return $this->caratula;
    }

    public function setCaratula(?string $caratula): self
    {
        $this->caratula = strtoupper($caratula);

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return Collection|Movimiento[]
     */
    public function getMovimientos(): Collection
    {
        return $this->movimientos;
    }

    public function addMovimiento(Movimiento $movimiento): self
    {
        if (!$this->movimientos->contains($movimiento)) {
            $this->movimientos[] = $movimiento;
            $movimiento->setExpediente($this);
        }

        return $this;
    }

    public function removeMovimiento(Movimiento $movimiento): self
    {
        if ($this->movimientos->removeElement($movimiento)) {
            // set the owning side to null (unless already changed)
            if ($movimiento->getExpediente() === $this) {
                $movimiento->setExpediente(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSolicitante(): ?Persona
    {
        return $this->solicitante;
    }

    public function setSolicitante(?Persona $solicitante): self
    {
        $this->solicitante = $solicitante;

        return $this;
    }

    public function getArchivo(): ?string
    {
        return $this->archivo;
    }

    public function setArchivo(?string $archivo): self
    {
        $this->archivo = $archivo;

        return $this;
    }

    public function getJuzgado(): ?Juzgado
    {
        return $this->juzgado;
    }

    public function setJuzgado(?Juzgado $juzgado): self
    {
        $this->juzgado = $juzgado;

        return $this;
    }

    public function getArea(): ?Area
    {
        return $this->area;
    }

    public function setArea(?Area $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
