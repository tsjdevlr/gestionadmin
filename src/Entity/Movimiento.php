<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table("movimientos")
 * @ORM\Entity(repositoryClass="App\Repository\MovimientoRepository")
 * @Gedmo\Loggable
 */
class Movimiento
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Expediente::class, inversedBy="movimientos", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Expediente;

    /**
     * @ORM\ManyToOne(targetEntity=Area::class, inversedBy="movimientos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Area;
    
    /**
     * @var string $fechaIngreso
     * @ORM\Column(name="fechaIngreso", type="datetime", nullable=true)
     * 
     */
    
    protected $fechaIngreso;

    /**
     * @var string $fechaEgreso
     * @ORM\Column(name="fechaEgreso", type="datetime", nullable=true)
     * 
     */
    
    protected $fechaEgreso;

    /**
     * @var string $observacion
     * @ORM\Column(name="observacion", type="string", nullable=true, length=500)
     * 
     */
    
    protected $observacion;

    /**
     * @var string $estado
     * @ORM\Column(name="estado", type="string", nullable=true, length=50)
     */
    
    protected $estado;

    /**
     * @var string $ubicacion
     * @ORM\Column(name="ubicacion", type="string", nullable=true, length=50)
     */
    
    protected $ubicacion;

    /**
    * @var string
    *
    * @ORM\Column(name="archivo", type="string", nullable=true)
    */
    protected $archivo;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="movimientos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
      * @var \DateTime
      *
      * @ORM\Column(name="createdAt", type="datetime")
      * @Gedmo\Timestampable(on="create")
      */
      private $createdAt;

      /**
      * @var \DateTime
      *
      * @ORM\Column(name="updatedAt", type="datetime")
      * @Gedmo\Timestampable(on="update")
      * @Gedmo\Versioned
      */
    private $updatedAt;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExpediente(): ?Expediente
    {
        return $this->Expediente;
    }

    public function setExpediente(?Expediente $Expediente): self
    {
        $this->Expediente = $Expediente;

        return $this;
    }

    public function getFechaIngreso(): ?\DateTimeInterface
    {
        return $this->fechaIngreso;
    }

    public function setFechaIngreso(?\DateTimeInterface $fechaIngreso): self
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    public function getFechaEgreso(): ?\DateTimeInterface
    {
        return $this->fechaEgreso;
    }

    public function setFechaEgreso(?\DateTimeInterface $fechaEgreso): self
    {
        $this->fechaEgreso = $fechaEgreso;

        return $this;
    }

    public function getArea(): ?Area
    {
        return $this->Area;
    }

    public function setArea(?Area $Area): self
    {
        $this->Area = $Area;

        return $this;
    }

    public function getObservacion(): ? string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = strtoupper($observacion);

        return $this;
    }

    public function getEstado(): ? string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUbicacion(): ?string
    {
        return $this->ubicacion;
    }

    public function setUbicacion(?string $ubicacion): self
    {
        $this->ubicacion = $ubicacion;

        return $this;
    }

    public function getArchivo(): ?string
    {
        return $this->archivo;
    }

    public function setArchivo(?string $archivo): self
    {
        $this->archivo = $archivo;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
    
}
