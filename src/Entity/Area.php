<?php

namespace App\Entity;

use App\Entity\Area\Grupo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table("areas")
 * @ORM\Entity(repositoryClass="App\Repository\AreaRepository")
 * @Gedmo\Loggable
 */
class Area
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string", nullable=false, length=255)
     * @Gedmo\Versioned
     * @Assert\NotBlank(message="Por favor ingrese Area.") 
     */
    protected $nombre;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    private $lft;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rgt;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Area", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $parent;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer",nullable=true)
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity="Area", mappedBy="parent")
     */
    private $children;

     /**
     * @ORM\OneToMany(targetEntity=Firma::class, mappedBy="area")
     */
    private $firmas;

    /**
     * @ORM\OneToMany(targetEntity=Expediente::class, mappedBy="area")
     */
    private $expedientes;

    /**
     * @ORM\OneToMany(targetEntity=Movimiento::class, mappedBy="Area")
     * @ORM\OrderBy({"fechaEgreso" = "ASC"})
     */
    private $movimientos;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="area")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity=Grupo::class, mappedBy="areas")
     */
    private $grupos;

    public function getNombreTree()
    {
        $elemento = $this;
        $value = $elemento->getNombre();

        if ($elemento->getLevel()) {
            while($elemento->getParent()) {
                $elemento = $elemento->getParent();
                $value = $elemento->getNombre().' > '.$value;
            }
        }

        return $value;
    }

    public function __toString()
    {
        return $this->getNombreTree();
    }

    public function __construct()
    {
        $this->movimientos = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->grupos = new ArrayCollection();
        $this->expedientes = new ArrayCollection();
        $this->firmas = new ArrayCollection();
    }
    
   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    /**
     * @return Collection|Movimiento[]
     */
    public function getMovimientos(): Collection
    {
        return $this->movimientos;
    }

    public function addMovimiento(Movimiento $movimiento): self
    {
        if (!$this->movimientos->contains($movimiento)) {
            $this->movimientos[] = $movimiento;
            $movimiento->setAreas($this);
        }

        return $this;
    }

    public function removeMovimiento(Movimiento $movimiento): self
    {
        if ($this->movimientos->removeElement($movimiento)) {
            // set the owning side to null (unless already changed)
            if ($movimiento->getAreas() === $this) {
                $movimiento->setAreas(null);
            }
        }

        return $this;
    }

    public function getLft(): ?int
    {
        return $this->lft;
    }

    public function setLft(int $lft): self
    {
        $this->lft = $lft;

        return $this;
    }

    public function getRgt(): ?int
    {
        return $this->rgt;
    }

    public function setRgt(int $rgt): self
    {
        $this->rgt = $rgt;

        return $this;
    }

    public function getRoot(): ?int
    {
        return $this->root;
    }

    public function setRoot(?int $root): self
    {
        $this->root = $root;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Area[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(Area $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(Area $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setArea($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getArea() === $this) {
                $user->setArea(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Grupo[]
     */
    public function getGrupos(): Collection
    {
        return $this->grupos;
    }

    public function addGrupo(Grupo $grupo): self
    {
        if (!$this->grupos->contains($grupo)) {
            $this->grupos[] = $grupo;
            $grupo->addArea($this);
        }

        return $this;
    }

    public function removeGrupo(Grupo $grupo): self
    {
        if ($this->grupos->removeElement($grupo)) {
            $grupo->removeArea($this);
        }

        return $this;
    }

    /**
     * @return Collection|Expediente[]
     */
    public function getExpedientes(): Collection
    {
        return $this->expedientes;
    }

    public function addExpediente(Expediente $expediente): self
    {
        if (!$this->expedientes->contains($expediente)) {
            $this->expedientes[] = $expediente;
            $expediente->setArea($this);
        }

        return $this;
    }

    public function removeExpediente(Expediente $expediente): self
    {
        if ($this->expedientes->removeElement($expediente)) {
            // set the owning side to null (unless already changed)
            if ($expediente->getArea() === $this) {
                $expediente->setArea(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Firma[]
     */
    public function getFirmas(): Collection
    {
        return $this->firmas;
    }

    public function addFirma(Firma $firma): self
    {
        if (!$this->firmas->contains($firma)) {
            $this->firmas[] = $firma;
            $firma->setArea($this);
        }

        return $this;
    }

    public function removeFirma(Firma $firma): self
    {
        if ($this->firmas->removeElement($firma)) {
            // set the owning side to null (unless already changed)
            if ($firma->getArea() === $this) {
                $firma->setArea(null);
            }
        }

        return $this;
    }
}
