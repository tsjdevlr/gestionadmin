<?php

namespace App\Form;

use App\Entity\Movimiento;
use App\Entity\Expediente;
use App\Entity\Area;
use App\Entity\Firma;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType; 
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Security\Core\Security; 


class PaseFirmadoType extends AbstractType
{
    private $security;
    private $user;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $this->security->getUser();

        $builder
            ->add('Area', EntityType::class, [
                'class' => Area::class,
                'query_builder' => function (EntityRepository $er) {
                    if($this->user->getArea()){
                        if($this->user->getArea()->getParent()){                         
                            $db = $er->createQueryBuilder('a');
                            if($this->user->getArea()->getParent()){
                                $db->Where("a.parent = :area")
                                    ->orWhere("a.id = :area")                                
                                    ->setParameter("area", $this->security->getUser()->getArea()->getParent());     
                            }else{
                                $db->Where("a.parent = :area")
                                    ->orWhere("a.id = :area")                                
                                    ->setParameter("area", $this->security->getUser()->getArea());
                            }
                            return $db;
                        }else{
                            $db = $er->createQueryBuilder('a');
                        }                              
                    }else{
                        return $er->createQueryBuilder('a');
                    } 
                },
                'choice_label' => function(?Area $area) {
                    if($area->getParent()){
                        return $area->getParent()->getNombre()." >> ".$area->getNombre();  
                    }
                    return $area->getNombre();
                },
            ])
            ->add('estado', ChoiceType::class, [
                'choices'  => [
                    'Aprobado' => 'Aprobado',
                    'No Aprobado' => 'No Aprobado',
                    //'En Proceso' => 'En Proceso',
                    'En Revisión' => 'En Revisión',
                    'Archivado' => 'Archivado',
                    'Finalizado' => 'Finalizado',
                    'SEF' => 'SEF',
                ]])
            ->add('observacion',TextareaType::class, [
                'label' => 'Observación',
                'required' => false,
            ])
            ->add('firma',EntityType::class, [
                'class' => Firma::class,
                //'placeholder' => 'Ninguno',
                'label' => 'Firmado Por',
                'query_builder' => function (EntityRepository $er) {
                    if($this->user->getArea() and $this->user->getArea()->getNombre() == 'SECRETARIA ADMINISTRATIVA'){           
                            $db = $er->createQueryBuilder('a');
                            $db->Where("a.area = :area")                                
                               ->setParameter("area", $this->security->getUser()->getArea());     
                            
                            return $db;
                    } 
                },
                'choice_label' => function(?Firma $firma) {
                    return $firma->getPersona()->getLastName().", ".$firma->getPersona()->getNombre();
                },
                'required' => true,
              ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
      $resolver->setDefaults(array(
          'csrf_protection' => false,
      ));
    }
}
