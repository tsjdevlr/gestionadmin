<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Area;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Core\Security;

class UserAreaType extends AbstractType
{
    private $security;
    private $user;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $this->security->getUser();
        $builder
          ->add('name', null, [
            "label" => "Nombre"
          ])
          ->add('lastName', null, [
            "label" => "Apellido"
          ])
          ->add('telephone', null, [
            "label" => "Teléfono"
          ])
          ->add('cellphone', null, [
            "label" => "Celular"
          ])
          ->add('email')
          ->add('username')
          ->add('Area', EntityType::class, [
            'class' => Area::class,
            'query_builder' => function (EntityRepository $er) {
                if(in_array("ROLE_AREA_ADMIN", $this->user->getRoles())){
                  if($this->user->getArea()){
                      $db = $er->createQueryBuilder('a');
                      $db->Where("a.parent = :area")
                         //->orWhere("a.id = :area")                                
                         ->setParameter("area", $this->user->getArea());
                      return $db;
                      
                  }
                }else{
                  return $er->createQueryBuilder('a');
                }
                 
            },
            'choice_label' => function(?Area $area) {
                if($area->getParent()){
                    return $area->getParent()->getNombre()." >> ".$area->getNombre();  
                }
                return $area->getNombre();
            },
            'required' => true,
        ])
          ->add('password', RepeatedType::class, [
              'type' => PasswordType::class,
              'invalid_message' => 'Los password deben ser iguales.',
              'options' => ['attr' => ['class' => 'password-field']],
              'required' => true,
              'first_options'  => ['label' => 'Contraseña'],
              'second_options' => ['label' => 'Repita Contraseña'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
