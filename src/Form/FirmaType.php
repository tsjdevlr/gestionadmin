<?php

namespace App\Form;

use App\Entity\Firma;
use App\Entity\Persona;
use App\Entity\Area;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FirmaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        /*
            ->add('archivo', FileType::class, [
                'label' => 'Subir Firma (PNG/JPG)',
                'mapped' => false,
                'required' => true,/*
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Cargue un Imagen PDF válido',
                    ])
                ],
            ])*/
            ->add('area',EntityType::class, [
                'class' => Area::class,
                'placeholder' => 'Ninguno',
                'choice_label' => 'nombre',
                'label' => '',
                'required' => true,
              ])/*
              ->add('persona',EntityType::class, [
                'class' => Persona::class,
                'placeholder' => 'Ninguno',
                'choice_label' => function(?Persona $p) {
                    return $p->getLastName().", ".$p->getNombre();  
                },
                'label' => 'Persona',
                'required' => true,
              ])  */
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Firma::class,
        ]);
    }
}
