<?php

namespace App\Form;

use App\Entity\Persona;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',null, [
                "label" => "Nombre"
              ])
            ->add('lastName',null, [
                "label" => "Apellidos"
              ])
            ->add('dni',null, [
                "label" => "N° Dni"
              ])
            ->add('telephone',null, [
                "label" => "Telefono Fijo"
              ])
            ->add('cellphone',null, [
                "label" => "Telefono Celular"
              ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Persona::class,
        ]);
    }
}
