<?php

namespace App\Form\Expediente;

use App\Entity\Expediente;
use App\Entity\Area;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

/*
* Esta clase define los campos que se utilizarán en el filtro
*/
class FiltroSecAdminType extends AbstractType
{
    private $security;
    private $user;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

      $this->user = $this->security->getUser();

      $builder
          // Por ejemplo, si se quiere agregar el filtro por el nombre:
          ->add("numero", null,[
            'label' => 'Número',
            'required' => false,
          ])
          ->add("caratula", null,[
            'label' => 'Carátula',
            'required' => false,
          ])
          ->add("letra", null, [
              'label' => 'Letra',
              'required' => false,
          ])
          ->add('ubicacion', EntityType::class, [
            'label' => 'Ubicación Actual',
            'class' => Area::class,
            'placeholder' => 'Ninguno',
            'choice_label' => function(?Area $area) {
                if($area->getParent()){
                    return $area->getParent()->getNombre()." >> ".$area->getNombre();  
                }
                return $area->getNombre();
            },
            'attr' => array('style' => 'width:100% import!;'),
            'required' => false,
        ])
          ->setMethod("GET")
      ;
    }

    public function getName()
    {
        return 'filtro';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
      $resolver->setDefaults(array(
          'csrf_protection' => false,
      ));
    }
}
