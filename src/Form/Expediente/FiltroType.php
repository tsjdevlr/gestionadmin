<?php

namespace App\Form\Expediente;

use App\Entity\Expediente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/*
* Esta clase define los campos que se utilizarán en el filtro
*/
class FiltroType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
          // Por ejemplo, si se quiere agregar el filtro por el nombre:
          ->add("numero", null,[
            'label' => 'Número',
          ])
          ->add("caratula", null,[
            'label' => 'Carátula',
          ])
          ->add("letra")
          ->setMethod("GET")
      ;
    }

    public function getName()
    {
        return 'filtro';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
      $resolver->setDefaults(array(
          'csrf_protection' => false,
      ));
    }
}
