<?php

namespace App\Form\Expediente;

use App\Entity\Expediente;
use App\Entity\Juzgado;
use App\Entity\Area;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class FiltroEstadisticasType extends AbstractType
{   

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaIncial', DateType::class, [
                'label' => 'Desde',
                'widget' => 'choice',
                'placeholder' => [
                    'day' => 'Día', 'month' => 'Mes', 'year' => 'Año',
                ],                
                'format' => 'dd-MM-yyyy',
                'required' => True,
                'years' => range(2020,2030),  
                'required' => false,          
            ])
            ->add('fechaFin', DateType::class, [
                'label' => 'Hasta',
                'widget' => 'choice',
                'placeholder' => [
                    'day' => 'Día', 'month' => 'Mes', 'year' => 'Año',
                ],                
                'format' => 'dd-MM-yyyy',
                'required' => True,
                'years' => range(2020,2030),  
                'required' => false,          
            ])
            ->add('juzgado',EntityType::class, [
                'class' => Juzgado::class,
                'placeholder' => 'Ninguno',
                'choice_label' => 'nombre',
                'label' => 'Tribunal',
                'required' => false,
                'attr' => array('style' => 'width:100% import!;'),
            ])
            ->add('area',EntityType::class, [
                'class' => Area::class,
                'placeholder' => 'Ninguno',
                'choice_label' => 'nombre',
                'label' => 'Area',
                'required' => false,
                'attr' => array('style' => 'width:100% import!;'),
            ])
            ->add('nombre',null, [
                "label" => "Nombre Solicitante",
                'required' => false,
              ])
            ->add('apellidos',null, [
                "label" => "Apellidos Solicitante",
                'required' => false,
              ])
            ->add('dni',null, [
                "label" => "Dni Solicitante",
                'required' => false,
              ])
            ->setMethod("GET")
        ;
    }

    public function getName()
    {
        return 'filtro';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
      $resolver->setDefaults(array(
          'csrf_protection' => false,
      ));
    }
}
