<?php

namespace App\Form;

use App\Entity\Movimiento;
use App\Entity\Expediente;
use App\Entity\Area;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Security\Core\Security;


class MovimientoEditType extends AbstractType
{
    private $security;
    private $user;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $this->security->getUser();

        $builder
            ->add('Expediente', EntityType::class, [
                'class' => Expediente::class,
                'placeholder' => 'Ninguno',
                'choice_label' => 'numero',
                'label' => 'Expte. N°',
                'disabled' => true,
                'required' => true,
            ])
            ->add('Area', EntityType::class, [
                'class' => Area::class,
                'query_builder' => function (EntityRepository $er) {
                    if($this->user->getArea()){
                        if($this->user->getArea()->getParent()){                         
                            $db = $er->createQueryBuilder('a');
                            if($this->user->getArea()->getParent()){
                                $db->Where("a.parent = :area")
                                ->orWhere("a.id = :area")                                
                                ->setParameter("area", $this->security->getUser()->getArea()->getParent());     
                            }else{
                                $db->Where("a.parent = :area")
                                    ->orWhere("a.id = :area")                                
                                    ->setParameter("area", $this->security->getUser()->getArea());
                            }
                            return $db;
                        }else{
                            $db = $er->createQueryBuilder('a');
                        }                              
                    }else{
                        return $er->createQueryBuilder('a');
                    } 
                },
                'choice_label' => function(?Area $area) {
                    if($area->getParent()){
                        return $area->getParent()->getNombre()." >> ".$area->getNombre();  
                    }
                    return $area->getNombre();
                },
            ])
            /*->add('Area',EntityType::class, [
                'class' => Area::class,
                'placeholder' => 'Ninguno',
                'choice_label' => 'nombre',
                'label' => 'Area',
                'required' => true,
            ])
            ->add('fechaEgreso', Type\DateType::class, [
                'label' => 'Fecha de Egreso',
                'widget' => 'choice',
                'placeholder' => [
                    'day' => 'Día', 'month' => 'Mes', 'year' => 'Año',
                ],                
                'format' => 'dd-MM-yyyy',
                'required' => True,
                'years' => range(2000,2022),
                'data' => new \DateTime()               
            ])*/
            ->add('estado', ChoiceType::class, [
                'choices'  => [
                    'Aprobado' => 'Aprobado',
                    'No Aprobado' => 'No Aprobado',
                    //'En Proceso' => 'En Proceso',
                    'En Revisión' => 'En Revisión',
                    'Archivado' => 'Archivado',
                    'Finalizado' => 'Finalizado',
                ]])
            ->add('observacion',TextareaType::class, [
                    'label' => 'Observación',
                    'required' => false,
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Movimiento::class,
        ]);
    }
}
