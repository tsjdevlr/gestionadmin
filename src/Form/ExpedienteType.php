<?php

namespace App\Form;

use App\Entity\Expediente;
use App\Entity\Juzgado;
use App\Entity\Area;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ExpedienteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero', null,[
                'required' => True,
            ])
            ->add('letra', null,[
                'required' => True,
            ])
            ->add('caratula',TextareaType::class, [
                "label" => "Carátula",
                'required' => True,
              ])
            ->add('fecha', Type\DateType::class, [
                'label' => 'Fecha de Inicio',
                'widget' => 'choice',
                'placeholder' => [
                    'day' => 'Día', 'month' => 'Mes', 'year' => 'Año',
                ],                
                'format' => 'dd-MM-yyyy',
                'required' => True,
                'years' => range(2000,2022),
                'data' => new \DateTime()               
            ])
            ->add('juzgado',EntityType::class, [
                'class' => Juzgado::class,
                'placeholder' => 'Ninguno',
                'choice_label' => 'nombre',
                'label' => 'Tribunal',
                'required' => false,
              ])
            ->add('area',EntityType::class, [
                'class' => Area::class,
                'placeholder' => 'Ninguno',
                'choice_label' => 'nombre',
                'label' => 'Area',
                'required' => false,
              ])  
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Expediente::class,
        ]);
    }
}
