<?php

namespace App\Repository;

use App\Entity\Firma;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Firma|null find($id, $lockMode = null, $lockVersion = null)
 * @method Firma|null findOneBy(array $criteria, array $orderBy = null)
 * @method Firma[]    findAll()
 * @method Firma[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FirmaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Firma::class);
    }

    /*
    * Este metodo devuelve las entidades en el listado.
    * Como parametro recibe un array con los filtros que se
    * aplicaran
    */
    public function findForActionIndex($filtro = [])
    {
      $qb = $this->createQueryBuilder('e');

      // Este es un ejemplo de como se aplica un filtro.
      // El indice del array de $filtro hace referencia al valor que se aplicará
      // en este filtro si es que esta definido en el array y si no viene vacío
      /*if(isset($filtro["nombre"]) && $filtro["nombre"] != '') {
        $qb
          ->andWhere("e.nombre like :nombre")
          ->setParameter("nombre", '%'.$filtro["nombre"].'%')
        ;
      }*/

      return $qb;
    }

    // /**
    //  * @return Firma[] Returns an array of Firma objects
    //  */
    
    public function findByArea($value)
    {
        return $this->createQueryBuilder('f')
            ->innerJoin('f.area', 'a')
            ->andWhere('a.id = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findPersonaArea($filtro = [])
    {
        $qb = $this->createQueryBuilder('e');
        
        $qb
            ->innerJoin('e.persona', 'p')
            ->innerJoin('e.area', 'a')
            ->andWhere("p.id = :persona")
            ->setParameter("persona", $filtro["persona"])
            ->andWhere("a.id = :area")
            ->setParameter("area", $filtro["area"])
          ;
        

        return $qb->getQuery()->getResult();
    }
    /*
    public function findOneBySomeField($value): ?Firma
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
