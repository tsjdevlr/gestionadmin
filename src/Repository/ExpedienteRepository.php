<?php

namespace App\Repository;

use App\Entity\Expediente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\Security;

/**
 * @method Expediente|null find($id, $lockMode = null, $lockVersion = null)
 * @method Expediente|null findOneBy(array $criteria, array $orderBy = null)
 * @method Expediente[]    findAll()
 * @method Expediente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpedienteRepository extends ServiceEntityRepository
{
    private $security;
    private $user;

    public function __construct(RegistryInterface $registry, Security $security)
    {
        parent::__construct($registry, Expediente::class);
        $this->security = $security;
    }

    /*
    * Este metodo devuelve las entidades en el listado.
    * Como parametro recibe un array con los filtros que se
    * aplicaran
    */
    public function findForActionIndex($filtro = [])
    {
      $qb = $this->createQueryBuilder('e');
      
      if($this->security->getUser()->getArea()){
          if($this->security->getUser()->getArea()->getNombre() != 'SECRETARIA ADMINISTRATIVA'){
            $qb
              ->innerJoin('e.movimientos', 'm')
              ->andWhere("m.Area = :area")
              ->setParameter("area", $this->security->getUser()->getArea())
            ;
          
          }
      }
      
      // Este es un ejemplo de como se aplica un filtro.
      // El indice del array de $filtro hace referencia al valor que se aplicará
      // en este filtro si es que esta definido en el array y si no viene vacíozzzzzz
      if(isset($filtro["numero"]) && $filtro["numero"] != '') {
        $qb
          ->andWhere("e.numero like :numero")
          ->setParameter("numero", '%'.$filtro["numero"].'%')
        ;
      }
      if(isset($filtro["caratula"]) && $filtro["caratula"] != '') {
        $qb
          ->andWhere("e.caratula like :caratula")
          ->setParameter("caratula", '%'.$filtro["caratula"].'%')
        ;
      }

      if(isset($filtro["letra"]) && $filtro["letra"] != '') {
        $qb
          ->andWhere("e.letra = :letra")
          ->setParameter("letra", $filtro["letra"])
        ;
      }

      if(isset($filtro["ubicacion"]) && $filtro["ubicacion"] != '') {
        $qb
          ->innerJoin('e.movimientos', 'm')
          ->andWhere("m.ubicacion = :con")
          ->setParameter("con", 1)
          ->innerJoin('m.Area', 'a')
          ->andWhere("a.id = :ubicacion")
          ->setParameter("ubicacion", $filtro["ubicacion"])
        ;
      }

      return $qb->orderBy('e.fecha', 'DESC');;
    }

    public function findForEstadisticas($filtro = [])
    {
      $qb = $this->createQueryBuilder('e');     
      
      if(isset($filtro["juzgado"]) && $filtro["juzgado"] != '') {
        $qb
          ->innerJoin('e.juzgado', 'j')
          ->andWhere("j.id = :juzgado")
          ->setParameter("juzgado", $filtro["juzgado"])
        ;
      }      

      if(isset($filtro["area"]) && $filtro["area"] != '') {
        $qb
          ->innerJoin('e.area', 'a')
          ->andWhere("a.id = :area")
          ->setParameter("area", $filtro["area"])
        ;
      }

      if(isset($filtro["fechaIncial"]) && $filtro["fechaIncial"] != '') {
        $qb
          ->andWhere("e.fecha >= :fecha")
          ->setParameter("fecha", $filtro["fechaIncial"])
        ;
      }

      if(isset($filtro["fechaFin"]) && $filtro["fechaFin"] != '') {
        $qb
          ->andWhere("e.fecha <= :fecha")
          ->setParameter("fecha", $filtro["fechaFin"])
        ;
      }

      if((isset($filtro["nombre"]) && $filtro["nombre"] != '') or (isset($filtro["apellidos"]) && $filtro["apellidos"] != '') or (isset($filtro["dni"]) && $filtro["dni"] != '')){
        $qb
          ->innerJoin('e.solicitante', 's');
        
      }

      if(isset($filtro["nombre"]) && $filtro["nombre"] != '') {
        $qb
          ->andWhere("s.nombre like :nombre")
          ->setParameter("nombre", '%'.$filtro["nombre"].'%')
        ;
      }
      if(isset($filtro["apellidos"]) && $filtro["apellidos"] != '') {
        $qb
          ->andWhere("s.lastName like :apellidos")
          ->setParameter("apellidos", '%'.$filtro["apellidos"].'%')
        ;
      }
      if(isset($filtro["dni"]) && $filtro["dni"] != '') {
        $qb
          ->andWhere("s.dni like :dni")
          ->setParameter("dni", '%'.$filtro["dni"].'%')
        ;
      }

      return $qb->orderBy('e.fecha', 'DESC')->getQuery()->getResult();
    }

    // /**
    //  * @return Expediente[] Returns an array of Expediente objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Expediente
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
