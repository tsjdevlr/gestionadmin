<?php

namespace App\Repository;

use App\Entity\Juzgado;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Juzgado|null find($id, $lockMode = null, $lockVersion = null)
 * @method Juzgado|null findOneBy(array $criteria, array $orderBy = null)
 * @method Juzgado[]    findAll()
 * @method Juzgado[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JuzgadoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Juzgado::class);
    }

    /*
    * Este metodo devuelve las entidades en el listado.
    * Como parametro recibe un array con los filtros que se
    * aplicaran
    */
    public function findForActionIndex($filtro = [])
    {
      $qb = $this->createQueryBuilder('e');

      // Este es un ejemplo de como se aplica un filtro.
      // El indice del array de $filtro hace referencia al valor que se aplicará
      // en este filtro si es que esta definido en el array y si no viene vacío
      /*if(isset($filtro["nombre"]) && $filtro["nombre"] != '') {
        $qb
          ->andWhere("e.nombre like :nombre")
          ->setParameter("nombre", '%'.$filtro["nombre"].'%')
        ;
      }*/

      return $qb;
    }

    // /**
    //  * @return Juzgado[] Returns an array of Juzgado objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Juzgado
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
