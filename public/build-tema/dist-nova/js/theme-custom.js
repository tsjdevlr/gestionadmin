$(function(){

    /*********PARA FILTRAR LOS OPTION EN LOS SELECT DEPENDIENTES*************************/
    // inicio los elementos select
    $('[data-filtro]').each(function(index, element){
        filtroEjecutar($(element), 'load');
    })

    // cuando cambia el elemento select    
    $('[data-filtro]').on('change', function(){
        filtroEjecutar($(this), 'change')
    })

    function filtroEjecutar($this, evento) {

        const valorParaFiltarResultados = $this.val();
        const $elementoParaFiltrar = $('[data-filtro-id="' + $this.data('filtro') + '"]')

        // habilito todo los elementos
        $elementoParaFiltrar.children('option').prop('disabled', false)

        // si viene un valor para filtrar los resultados o esta activado para que no muestre
        // options si no se selecciono ningún valor del select del cual depende        
        if ($elementoParaFiltrar.data("filtroNull") != undefined || valorParaFiltarResultados) {
            $elementoParaFiltrar.children('option:not([data-filtro-valor="' + valorParaFiltarResultados + '"])').prop('disabled', true)
            $elementoParaFiltrar.children('option[value=""]').prop('disabled', false)
        }

        if (evento != 'load') {
            // para que al cambiar no quede seleccionado el elemento        
            $elementoParaFiltrar.children('option[value=""]').prop('selected', true)          
        }
        
        // cargo el plugin select 2 en el elemento        
        $.HSCore.components.HSSelect2.init($elementoParaFiltrar);
    }
    /**************************************************************************************/    
});